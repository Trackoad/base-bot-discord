export class GlobalErrorException extends Error {

    constructor(message?: string | undefined) {
        super(message);
    }
}