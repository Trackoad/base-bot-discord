import { CommandInterfaceModel } from "../model/command-model";
import { DiscordService } from "./discord-service";
import Discord = require('discord.js');
import { CommandsList } from "../model/commands/commands";
import { LoggerService } from "./logger-service";
import { ConfigService } from "./config-service";

export class CommandeService {

    private static SEPARATOR_COMMAND_ARGS = ' ';

    private static _instance: CommandeService;
    static get instance(): CommandeService {
        return CommandeService._instance ? CommandeService._instance : CommandeService._instance = new CommandeService();
    }

    private constructor() {
        DiscordService.instance.onMessage(message => this.executeCommand(message));
    }

    private executeCommand(message: Discord.Message) {

        if(message.author.bot){
            return;
        }

        const args = this.parseCommand(message.content);
        const starter = ConfigService.instance.configBase.commandStarter;
        let commandExecute = <string>args.shift();
        if(starter && commandExecute.charAt(0) !== starter){
            return;
        }
        commandExecute = commandExecute.slice(1);
        const command = this.findCommand(commandExecute);
        if (!command) {
            LoggerService.error(`Bad command ${commandExecute}`);
            message.delete();
            return
        }

        if (command.canDeleteMessage()) {
            message.delete();
        }

        for(const role of command.permissions()) {
            if(!role.hasPermission(message)){
                LoggerService.error(`No permission for ${role.roleName}`);
                return;
            }
        }

        const argsRegExp = command.argsRegex();
        if (args.length >= argsRegExp.length) {
            const noMatch = argsRegExp.map((regExp, index) => regExp.test(args[index], index)).filter(value => value);
            if (!noMatch.length) {
                command.execute(args, message);
            } else {
                noMatch.forEach(errorMessage => LoggerService.error(<string>errorMessage));
            }
        } else {
            LoggerService.error('Bad args number');
        }
    }

    private findCommand(commandName: string): CommandInterfaceModel {
        return CommandsList.filter(command => command.name() === commandName)[0];
    }

    private parseCommand(message: string): Array<string> {
        return message.split(CommandeService.SEPARATOR_COMMAND_ARGS).filter(arg => arg.trim().length);
    }
}