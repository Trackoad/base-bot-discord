import FileSystem = require('fs');

export class FileService {

    static readFile(path: string): string {

        return FileSystem.readFileSync(path).toString();
    }
}