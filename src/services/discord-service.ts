import Discord = require('discord.js');
import { GlobalErrorException } from '../utils/exception';
import { ConfigService } from './config-service';
import { CommandeService } from './command-service';
import { LoggerService } from './logger-service';
import { EventService } from './event-service';

export class DiscordService {

    private _client: Discord.Client;
    private get client(): Discord.Client {
        return this._client;
    }

    private static _instance: DiscordService;
    static get instance(): DiscordService {
        return this._instance;
    }

    static start(): void {
        if (this._instance) {
            throw new GlobalErrorException('Bad using');
        }
        DiscordService._instance = new DiscordService();
        CommandeService.instance;
        EventService.instance;
    }

    private constructor() {
        this._client = new Discord.Client();
        this.client
            .login(ConfigService.instance.configBase.token)
            .then(() => LoggerService.app('Bot started !'))
            .catch(() => LoggerService.error('Bot can\'t start'));        
    }

    onMessage(listener: (message: Discord.Message) => void) {
        this.client.on('message', listener);
    }

}