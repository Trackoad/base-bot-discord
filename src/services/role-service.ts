import { RoleModel } from "../model/role-model";

export class RoleService {

    static readonly ROLE_ADMIN = new RoleModel("471671229322100736");

    static readonly ROLE_OTHER = new RoleModel("602237331705167884");

}