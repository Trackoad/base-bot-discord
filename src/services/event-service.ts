export class EventService {

    private static _instance: EventService;
    static get instance(): EventService {
        return EventService._instance ? EventService._instance : EventService._instance = new EventService();
    }
}