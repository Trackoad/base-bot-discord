import Discord = require('discord.js');
import { GlobalErrorException } from '../utils/exception';

export class RoleModel {
    private _id: string;

    private _role: Discord.Role | undefined;
    
    get roleName(): string {
        return this._role instanceof Discord.Role ? this._role.name : '[Role not loaded]';
    }

    constructor(id: string) {
        this._id = id;
    }

    hasPermission(messageOrigin: Discord.Message): boolean {
        if(!this._role){
            this.loadRole(messageOrigin);
        }
        if(this._role instanceof Discord.Role){
            const role = this._role;
            return Boolean(messageOrigin.member.roles.filter(roleMember => roleMember.equals(role)).size);
        }
        return false;
    }

    private loadRole(messageOrigin: Discord.Message) {
        this._role = messageOrigin.guild.roles.get(this._id);
        if(!this._role){
            throw new GlobalErrorException("Bad rank");
        }
    }

}