import { CommandInterfaceModel } from "../command-model";
import { RegExpModel } from "../regexp-model";
import Discord = require('discord.js');
import { CommandsList } from "../commands/commands";
import { RoleModel } from "../role-model";
import { FileService } from "../../services/file-service";
import { RoleService } from "../../services/role-service";

export class HelpCommandModel implements CommandInterfaceModel {
    
    private _content: string | undefined;

    permissions(): RoleModel[] {
        return [
            RoleService.ROLE_ADMIN
        ];
    }

    canDeleteMessage(): boolean {
        return true;
    }

    name(): string {
        return 'help';
    }

    description(): string {
        return 'Send list of commands in DM';
    }

    argsRegex(): RegExpModel[] {
        return [];
    }

    execute(args: String[], message: Discord.Message): void {

        if (!this._content) {
            this._content = FileService.readFile('src/config/help.md');
        }

        let result = '';
        for (const command of CommandsList) {
            if (command.permissions().length === 0 || command.permissions().filter(role => role.hasPermission(message)).length) {
                let params = command.argsRegex().map(reg => reg.description).toString();
                if(params){
                    params = `\`[${params}]\` `;
                }
                let perms = command.permissions().map(perm => perm.roleName).toString();
                if(perms){
                    perms = `||${perms}|| `;
                }
                const description = command.description();
                result += ` - **${command.name()}** : ${params}${perms}${(params || perms) && description ? '*>' : ''} ${description}*\n`;
            }
        }

        if(result){
            message.author.send(this._content + result);
        }
    }
}