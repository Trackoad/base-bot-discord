import { CommandInterfaceModel } from "../command-model";
import Discord = require('discord.js');
import { RegExpService } from "../../services/regexp-service";
import { RegExpModel } from "../regexp-model";
import { RoleModel } from "../role-model";

export class TrucCommandModel implements CommandInterfaceModel {

    name(): string {
        return 'truc';
    }

    description(): string {
        return '';
    }

    permissions(): RoleModel[] {
        return [];
    }

    canDeleteMessage(): boolean {
        return true;
    }

    
    argsRegex(): RegExpModel[] {
        return [
            RegExpService.REGEXP_LETTER,
            RegExpService.REGEXP_NUMBER
        ];
    }

    execute(args: String[], message: Discord.Message): void {
        message.reply(`Your are so cute ${args[0]} :)`);
    }
}