import { CommandInterfaceModel } from "../command-model";
import Discord = require('discord.js');
import { RegExpModel } from "../regexp-model";
import { LoggerService } from "../../services/logger-service";
import { RoleModel } from "../role-model";

export class HelloCommandModel implements CommandInterfaceModel {

    permissions(): RoleModel[] {
        return [];
    }

    argsRegex(): RegExpModel[] {
        return [];
    }

    canDeleteMessage(): boolean {
        return false;
    }

    description(): string {
        return '';
    }

    name(): string {
        return 'huitre';
    }

    execute(args: string[], message?: Discord.Message | undefined): void {
        LoggerService.log("Hello world !");
    }

}