import { HelloCommandModel } from "./hello-command-model";
import { CommandInterfaceModel } from "../command-model";
import { TrucCommandModel } from "./truc-command-model";
import { HelpCommandModel } from "./help-command-model";


export const CommandsList: Array<CommandInterfaceModel> = [
    HelloCommandModel,
    TrucCommandModel,
    HelpCommandModel
].map(command => Object.create(command.prototype));