export class RegExpModel {

    private _regexp: RegExp;
    get regexp(): RegExp {
        return this._regexp;
    }

    private _description: string;
    get description(): string {
        return this._description;
    }
    
    constructor(regexp: RegExp, description: string) {
        this._regexp = regexp;
        this._description = description;
    }

    test(content: string, index: number): string | undefined {
        if(!this.regexp.test(content)){
            return `[Error args] : index ${index + 1} should be ${this.description}`;
        }
    }
}