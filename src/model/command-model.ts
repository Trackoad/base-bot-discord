import Discord = require('discord.js');
import { RegExpModel } from './regexp-model';
import { RoleModel } from './role-model';

export interface CommandInterfaceModel {
    
    name(): string;

    description(): string;

    argsRegex(): Array<RegExpModel>;

    permissions(): Array<RoleModel>;

    execute(args: Array<string>, message: Discord.Message): void;

    canDeleteMessage(): boolean;
    
}