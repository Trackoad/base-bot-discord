import { DiscordService } from "./services/discord-service";

export class Main {
    static start(args?: Array<string>) {
        DiscordService.start();
    }
}

Main.start();