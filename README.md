﻿# 😜 Base bot on Discord 💬

##Author
> Trackoad

### Tech

* [Discord](https://discordapp.com/) - Discord
* [Node.js] - du js

And of course **This project** is open source with a [https://gitlab.com/Trackoad/base-bot-discord] on GitLab.

### Installation

```
make

```

Insert your token bot in config/config.json


Controllers will be at the next object format : 
```
{
    name:"name of command",
    execute:()=>{}
}
```
in js file placed in controllers/command/

### Launch

**Debug Mode**

```
node --inspect-brk index.js
```

**Without Debug**

```
node index.js
```
